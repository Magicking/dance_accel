#from st3m.reactor import Responder
#import st3m.run
#
#class Example(Responder):
#    def __init__(self) -> None:
#        self._x = -20
#
#    def draw(self, ctx: Context) -> None:
#        # Paint the background red
#        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
#        ctx.rgb(255, 0, 0).rectangle(self._x, -20, 40, 40).fill()
#
#    def think(self, ins: InputState, delta_ms: int) -> None:
#        direction = ins.buttons.app # -1 (left), 1 (right), or 2 (pressed)
#
#        if direction == ins.buttons.PRESSED_LEFT:
#            self._x -= 20 * delta_ms / 1000
#        elif direction == ins.buttons.PRESSED_RIGHT:
#            self._x += 40 * delta_ms / 1000
#
#st3m.run.run_responder(Example())

# flow3r imports
from st3m import InputState, run
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.utils import tau
import leds

class DanceAccel(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._x = -20
        self.to_print = 1
        self.cur = 0

    def draw(self, ctx: Context) -> None:
        if self.to_print == 1:

            ctx.rgb(255, 0, 0).rectangle(-120, -120, 240, 240).fill()
        else:
            ctx.rgb(0, 0, 255).rectangle(-120, -120, 240, 240).fill()
        leds.update()

    ## Determine the direction of the accelerometer
    ## If the accelerometer is pointing up, it will draw image 1
    ## If the accelerometer is pointing down, it will draw image 2
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        i = self.cur
        if ins.imu.acc[0] > 0:
            self.to_print = 1
            leds.set_rgb(i % 40, 255, 0, 0)
            i += 1
        else:
            self.to_print = 2
            leds.set_rgb(i % 40, 0, 0, 255)
            i -= 1
        if i <= 0:
            self.cur = 0
        else:
            self.cur = i % 40
        leds.update()

if __name__ == '__main__':
    run.run_view(DanceAccel(ApplicationContext()))
